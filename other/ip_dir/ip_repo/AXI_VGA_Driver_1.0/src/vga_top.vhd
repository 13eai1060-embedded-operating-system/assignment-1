library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity vga_top is
    port
    (
        mclk    : in std_logic;

        HSYNC   : out std_logic;
        VSYNC   : out std_logic;

        RED     : out std_logic_vector(4 downto 0);
        GRN     : out std_logic_vector(5 downto 0);
        BLU     : out std_logic_vector(4 downto 0);

        -- AXI registers
        -- Row 0
        PIX_0_0 : in std_logic_vector(11 downto 0);
        PIX_0_1 : in std_logic_vector(11 downto 0);
        PIX_0_2 : in std_logic_vector(11 downto 0);
        PIX_0_3 : in std_logic_vector(11 downto 0);
        PIX_0_4 : in std_logic_vector(11 downto 0);
        PIX_0_5 : in std_logic_vector(11 downto 0);
        PIX_0_6 : in std_logic_vector(11 downto 0);
        PIX_0_7 : in std_logic_vector(11 downto 0);

        -- Row 1
        PIX_1_0 : in std_logic_vector(11 downto 0);
        PIX_1_1 : in std_logic_vector(11 downto 0);
        PIX_1_2 : in std_logic_vector(11 downto 0);
        PIX_1_3 : in std_logic_vector(11 downto 0);
        PIX_1_4 : in std_logic_vector(11 downto 0);
        PIX_1_5 : in std_logic_vector(11 downto 0);
        PIX_1_6 : in std_logic_vector(11 downto 0);
        PIX_1_7 : in std_logic_vector(11 downto 0);

        -- Row 2
        PIX_2_0 : in std_logic_vector(11 downto 0);
        PIX_2_1 : in std_logic_vector(11 downto 0);
        PIX_2_2 : in std_logic_vector(11 downto 0);
        PIX_2_3 : in std_logic_vector(11 downto 0);
        PIX_2_4 : in std_logic_vector(11 downto 0);
        PIX_2_5 : in std_logic_vector(11 downto 0);
        PIX_2_6 : in std_logic_vector(11 downto 0);
        PIX_2_7 : in std_logic_vector(11 downto 0);

        -- Row 3
        PIX_3_0 : in std_logic_vector(11 downto 0);
        PIX_3_1 : in std_logic_vector(11 downto 0);
        PIX_3_2 : in std_logic_vector(11 downto 0);
        PIX_3_3 : in std_logic_vector(11 downto 0);
        PIX_3_4 : in std_logic_vector(11 downto 0);
        PIX_3_5 : in std_logic_vector(11 downto 0);
        PIX_3_6 : in std_logic_vector(11 downto 0);
        PIX_3_7 : in std_logic_vector(11 downto 0);

        -- Row 4
        PIX_4_0 : in std_logic_vector(11 downto 0);
        PIX_4_1 : in std_logic_vector(11 downto 0);
        PIX_4_2 : in std_logic_vector(11 downto 0);
        PIX_4_3 : in std_logic_vector(11 downto 0);
        PIX_4_4 : in std_logic_vector(11 downto 0);
        PIX_4_5 : in std_logic_vector(11 downto 0);
        PIX_4_6 : in std_logic_vector(11 downto 0);
        PIX_4_7 : in std_logic_vector(11 downto 0);

        -- Row 5
        PIX_5_0 : in std_logic_vector(11 downto 0);
        PIX_5_1 : in std_logic_vector(11 downto 0);
        PIX_5_2 : in std_logic_vector(11 downto 0);
        PIX_5_3 : in std_logic_vector(11 downto 0);
        PIX_5_4 : in std_logic_vector(11 downto 0);
        PIX_5_5 : in std_logic_vector(11 downto 0);
        PIX_5_6 : in std_logic_vector(11 downto 0);
        PIX_5_7 : in std_logic_vector(11 downto 0);

        -- Row 6
        PIX_6_0 : in std_logic_vector(11 downto 0);
        PIX_6_1 : in std_logic_vector(11 downto 0);
        PIX_6_2 : in std_logic_vector(11 downto 0);
        PIX_6_3 : in std_logic_vector(11 downto 0);
        PIX_6_4 : in std_logic_vector(11 downto 0);
        PIX_6_5 : in std_logic_vector(11 downto 0);
        PIX_6_6 : in std_logic_vector(11 downto 0);
        PIX_6_7 : in std_logic_vector(11 downto 0);

        -- Row 7
        PIX_7_0 : in std_logic_vector(11 downto 0);
        PIX_7_1 : in std_logic_vector(11 downto 0);
        PIX_7_2 : in std_logic_vector(11 downto 0);
        PIX_7_3 : in std_logic_vector(11 downto 0);
        PIX_7_4 : in std_logic_vector(11 downto 0);
        PIX_7_5 : in std_logic_vector(11 downto 0);
        PIX_7_6 : in std_logic_vector(11 downto 0);
        PIX_7_7 : in std_logic_vector(11 downto 0)
    );
end vga_top;

architecture Behavioral of vga_top is
    -- Horizontal timing parameters
    -- Horizontal display width
    constant h_display : integer := 640;
    -- Horizontal front porch
    constant h_fp      : integer := 16;
    -- Horizontal sync pulse
    constant h_sync    : integer := 96;
    -- Horizontal back proch
    constant h_bp      : integer := 48;

    -- Vertical timing parameters
    -- Vertical display height
    constant v_display : integer := 480;
    -- Vertical front porch
    constant v_fp      : integer := 10;
    -- Vertical sync pulse
    constant v_sync    : integer := 2;
    -- Vertical back porch
    constant v_bp      : integer := 33;

    signal iclk        : std_logic;
    signal hc          : std_logic_vector(9 downto 0);
    signal vc          : std_logic_vector(9 downto 0);

    component clk_wiz_0
        port
        (
            clk_in1  : in std_logic;

            clk_out1 : out std_logic
        );
    end component clk_wiz_0;

begin
    U1 : clk_wiz_0
    port map
    (
        clk_in1  => mclk,
        clk_out1 => iclk
    );

    vga_logic : process (iclk)
    begin
        if (rising_edge(iclk)) then
            -- Main counters
            if (hc < (h_display + h_fp + h_sync + h_bp)) then
                hc <= hc + 1;
            else
                hc <= (others => '0');

                if (vc < (v_display + v_fp + v_sync + v_bp)) then
                    vc <= vc + 1;
                else
                    vc <= (others => '0');
                end if;
            end if;

            -- Horizontal sync pulse logic
            if ((hc >= (h_display + h_fp)) and (hc < (h_display + h_fp + h_sync))) then
                HSYNC <= '0';
            else
                HSYNC <= '1';
            end if;

            -- Vertical sync pulse logic
            if ((vc >= (v_display + v_fp)) and (vc < (v_display + v_fp + v_sync))) then
                VSYNC <= '0';
            else
                VSYNC <= '1';
            end if;

            if (hc > 80 and hc < 560 and vc < 480) then
                RED <= "00000";
                GRN <= "000000";
                BLU <= "00000";

                -- Row 0
                if ((vc > (0 + (60 * 0))) and (vc < (60 + (60 * 0))) and (hc > (80 + (60 * 0))) and (hc < (140 + (60 * 0)))) then
                    RED <= PIX_0_0(11 downto 8) & '0';
                    GRN <= PIX_0_0(7 downto 4) & "00";
                    BLU <= PIX_0_0(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 0))) and (vc < (60 + (60 * 0))) and (hc > (80 + (60 * 1))) and (hc < (140 + (60 * 1)))) then
                    RED <= PIX_0_1(11 downto 8) & '0';
                    GRN <= PIX_0_1(7 downto 4) & "00";
                    BLU <= PIX_0_1(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 0))) and (vc < (60 + (60 * 0))) and (hc > (80 + (60 * 2))) and (hc < (140 + (60 * 2)))) then
                    RED <= PIX_0_2(11 downto 8) & '0';
                    GRN <= PIX_0_2(7 downto 4) & "00";
                    BLU <= PIX_0_2(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 0))) and (vc < (60 + (60 * 0))) and (hc > (80 + (60 * 3))) and (hc < (140 + (60 * 3)))) then
                    RED <= PIX_0_3(11 downto 8) & '0';
                    GRN <= PIX_0_3(7 downto 4) & "00";
                    BLU <= PIX_0_3(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 0))) and (vc < (60 + (60 * 0))) and (hc > (80 + (60 * 4))) and (hc < (140 + (60 * 4)))) then
                    RED <= PIX_0_4(11 downto 8) & '0';
                    GRN <= PIX_0_4(7 downto 4) & "00";
                    BLU <= PIX_0_4(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 0))) and (vc < (60 + (60 * 0))) and (hc > (80 + (60 * 5))) and (hc < (140 + (60 * 5)))) then
                    RED <= PIX_0_5(11 downto 8) & '0';
                    GRN <= PIX_0_5(7 downto 4) & "00";
                    BLU <= PIX_0_5(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 0))) and (vc < (60 + (60 * 0))) and (hc > (80 + (60 * 6))) and (hc < (140 + (60 * 6)))) then
                    RED <= PIX_0_6(11 downto 8) & '0';
                    GRN <= PIX_0_6(7 downto 4) & "00";
                    BLU <= PIX_0_6(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 0))) and (vc < (60 + (60 * 0))) and (hc > (80 + (60 * 7))) and (hc < (140 + (60 * 7)))) then
                    RED <= PIX_0_7(11 downto 8) & '0';
                    GRN <= PIX_0_7(7 downto 4) & "00";
                    BLU <= PIX_0_7(3 downto 0) & '0';
                end if;

                -- Row 1
                if ((vc > (0 + (60 * 1))) and (vc < (60 + (60 * 1))) and (hc > (80 + (60 * 0))) and (hc < (140 + (60 * 0)))) then
                    RED <= PIX_1_0(11 downto 8) & '0';
                    GRN <= PIX_1_0(7 downto 4) & "00";
                    BLU <= PIX_1_0(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 1))) and (vc < (60 + (60 * 1))) and (hc > (80 + (60 * 1))) and (hc < (140 + (60 * 1)))) then
                    RED <= PIX_1_1(11 downto 8) & '0';
                    GRN <= PIX_1_1(7 downto 4) & "00";
                    BLU <= PIX_1_1(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 1))) and (vc < (60 + (60 * 1))) and (hc > (80 + (60 * 2))) and (hc < (140 + (60 * 2)))) then
                    RED <= PIX_1_2(11 downto 8) & '0';
                    GRN <= PIX_1_2(7 downto 4) & "00";
                    BLU <= PIX_1_2(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 1))) and (vc < (60 + (60 * 1))) and (hc > (80 + (60 * 3))) and (hc < (140 + (60 * 3)))) then
                    RED <= PIX_1_3(11 downto 8) & '0';
                    GRN <= PIX_1_3(7 downto 4) & "00";
                    BLU <= PIX_1_3(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 1))) and (vc < (60 + (60 * 1))) and (hc > (80 + (60 * 4))) and (hc < (140 + (60 * 4)))) then
                    RED <= PIX_1_4(11 downto 8) & '0';
                    GRN <= PIX_1_4(7 downto 4) & "00";
                    BLU <= PIX_1_4(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 1))) and (vc < (60 + (60 * 1))) and (hc > (80 + (60 * 5))) and (hc < (140 + (60 * 5)))) then
                    RED <= PIX_1_5(11 downto 8) & '0';
                    GRN <= PIX_1_5(7 downto 4) & "00";
                    BLU <= PIX_1_5(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 1))) and (vc < (60 + (60 * 1))) and (hc > (80 + (60 * 6))) and (hc < (140 + (60 * 6)))) then
                    RED <= PIX_1_6(11 downto 8) & '0';
                    GRN <= PIX_1_6(7 downto 4) & "00";
                    BLU <= PIX_1_6(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 1))) and (vc < (60 + (60 * 1))) and (hc > (80 + (60 * 7))) and (hc < (140 + (60 * 7)))) then
                    RED <= PIX_1_7(11 downto 8) & '0';
                    GRN <= PIX_1_7(7 downto 4) & "00";
                    BLU <= PIX_1_7(3 downto 0) & '0';
                end if;

                -- Row 2
                if ((vc > (0 + (60 * 2))) and (vc < (60 + (60 * 2))) and (hc > (80 + (60 * 0))) and (hc < (140 + (60 * 0)))) then
                    RED <= PIX_2_0(11 downto 8) & '0';
                    GRN <= PIX_2_0(7 downto 4) & "00";
                    BLU <= PIX_2_0(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 2))) and (vc < (60 + (60 * 2))) and (hc > (80 + (60 * 1))) and (hc < (140 + (60 * 1)))) then
                    RED <= PIX_2_1(11 downto 8) & '0';
                    GRN <= PIX_2_1(7 downto 4) & "00";
                    BLU <= PIX_2_1(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 2))) and (vc < (60 + (60 * 2))) and (hc > (80 + (60 * 2))) and (hc < (140 + (60 * 2)))) then
                    RED <= PIX_2_2(11 downto 8) & '0';
                    GRN <= PIX_2_2(7 downto 4) & "00";
                    BLU <= PIX_2_2(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 2))) and (vc < (60 + (60 * 2))) and (hc > (80 + (60 * 3))) and (hc < (140 + (60 * 3)))) then
                    RED <= PIX_2_3(11 downto 8) & '0';
                    GRN <= PIX_2_3(7 downto 4) & "00";
                    BLU <= PIX_2_3(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 2))) and (vc < (60 + (60 * 2))) and (hc > (80 + (60 * 4))) and (hc < (140 + (60 * 4)))) then
                    RED <= PIX_2_4(11 downto 8) & '0';
                    GRN <= PIX_2_4(7 downto 4) & "00";
                    BLU <= PIX_2_4(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 2))) and (vc < (60 + (60 * 2))) and (hc > (80 + (60 * 5))) and (hc < (140 + (60 * 5)))) then
                    RED <= PIX_2_5(11 downto 8) & '0';
                    GRN <= PIX_2_5(7 downto 4) & "00";
                    BLU <= PIX_2_5(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 2))) and (vc < (60 + (60 * 2))) and (hc > (80 + (60 * 6))) and (hc < (140 + (60 * 6)))) then
                    RED <= PIX_2_6(11 downto 8) & '0';
                    GRN <= PIX_2_6(7 downto 4) & "00";
                    BLU <= PIX_2_6(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 2))) and (vc < (60 + (60 * 2))) and (hc > (80 + (60 * 7))) and (hc < (140 + (60 * 7)))) then
                    RED <= PIX_2_7(11 downto 8) & '0';
                    GRN <= PIX_2_7(7 downto 4) & "00";
                    BLU <= PIX_2_7(3 downto 0) & '0';
                end if;

                -- Row 3
                if ((vc > (0 + (60 * 3))) and (vc < (60 + (60 * 3))) and (hc > (80 + (60 * 0))) and (hc < (140 + (60 * 0)))) then
                    RED <= PIX_3_0(11 downto 8) & '0';
                    GRN <= PIX_3_0(7 downto 4) & "00";
                    BLU <= PIX_3_0(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 3))) and (vc < (60 + (60 * 3))) and (hc > (80 + (60 * 1))) and (hc < (140 + (60 * 1)))) then
                    RED <= PIX_3_1(11 downto 8) & '0';
                    GRN <= PIX_3_1(7 downto 4) & "00";
                    BLU <= PIX_3_1(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 3))) and (vc < (60 + (60 * 3))) and (hc > (80 + (60 * 2))) and (hc < (140 + (60 * 2)))) then
                    RED <= PIX_3_2(11 downto 8) & '0';
                    GRN <= PIX_3_2(7 downto 4) & "00";
                    BLU <= PIX_3_2(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 3))) and (vc < (60 + (60 * 3))) and (hc > (80 + (60 * 3))) and (hc < (140 + (60 * 3)))) then
                    RED <= PIX_3_3(11 downto 8) & '0';
                    GRN <= PIX_3_3(7 downto 4) & "00";
                    BLU <= PIX_3_3(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 3))) and (vc < (60 + (60 * 3))) and (hc > (80 + (60 * 4))) and (hc < (140 + (60 * 4)))) then
                    RED <= PIX_3_4(11 downto 8) & '0';
                    GRN <= PIX_3_4(7 downto 4) & "00";
                    BLU <= PIX_3_4(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 3))) and (vc < (60 + (60 * 3))) and (hc > (80 + (60 * 5))) and (hc < (140 + (60 * 5)))) then
                    RED <= PIX_3_5(11 downto 8) & '0';
                    GRN <= PIX_3_5(7 downto 4) & "00";
                    BLU <= PIX_3_5(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 3))) and (vc < (60 + (60 * 3))) and (hc > (80 + (60 * 6))) and (hc < (140 + (60 * 6)))) then
                    RED <= PIX_3_6(11 downto 8) & '0';
                    GRN <= PIX_3_6(7 downto 4) & "00";
                    BLU <= PIX_3_6(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 3))) and (vc < (60 + (60 * 3))) and (hc > (80 + (60 * 7))) and (hc < (140 + (60 * 7)))) then
                    RED <= PIX_3_7(11 downto 8) & '0';
                    GRN <= PIX_3_7(7 downto 4) & "00";
                    BLU <= PIX_3_7(3 downto 0) & '0';
                end if;

                -- Row 4
                if ((vc > (0 + (60 * 4))) and (vc < (60 + (60 * 4))) and (hc > (80 + (60 * 0))) and (hc < (140 + (60 * 0)))) then
                    RED <= PIX_4_0(11 downto 8) & '0';
                    GRN <= PIX_4_0(7 downto 4) & "00";
                    BLU <= PIX_4_0(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 4))) and (vc < (60 + (60 * 4))) and (hc > (80 + (60 * 1))) and (hc < (140 + (60 * 1)))) then
                    RED <= PIX_4_1(11 downto 8) & '0';
                    GRN <= PIX_4_1(7 downto 4) & "00";
                    BLU <= PIX_4_1(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 4))) and (vc < (60 + (60 * 4))) and (hc > (80 + (60 * 2))) and (hc < (140 + (60 * 2)))) then
                    RED <= PIX_4_2(11 downto 8) & '0';
                    GRN <= PIX_4_2(7 downto 4) & "00";
                    BLU <= PIX_4_2(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 4))) and (vc < (60 + (60 * 4))) and (hc > (80 + (60 * 3))) and (hc < (140 + (60 * 3)))) then
                    RED <= PIX_4_3(11 downto 8) & '0';
                    GRN <= PIX_4_3(7 downto 4) & "00";
                    BLU <= PIX_4_3(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 4))) and (vc < (60 + (60 * 4))) and (hc > (80 + (60 * 4))) and (hc < (140 + (60 * 4)))) then
                    RED <= PIX_4_4(11 downto 8) & '0';
                    GRN <= PIX_4_4(7 downto 4) & "00";
                    BLU <= PIX_4_4(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 4))) and (vc < (60 + (60 * 4))) and (hc > (80 + (60 * 5))) and (hc < (140 + (60 * 5)))) then
                    RED <= PIX_4_5(11 downto 8) & '0';
                    GRN <= PIX_4_5(7 downto 4) & "00";
                    BLU <= PIX_4_5(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 4))) and (vc < (60 + (60 * 4))) and (hc > (80 + (60 * 6))) and (hc < (140 + (60 * 6)))) then
                    RED <= PIX_4_6(11 downto 8) & '0';
                    GRN <= PIX_4_6(7 downto 4) & "00";
                    BLU <= PIX_4_6(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 4))) and (vc < (60 + (60 * 4))) and (hc > (80 + (60 * 7))) and (hc < (140 + (60 * 7)))) then
                    RED <= PIX_4_7(11 downto 8) & '0';
                    GRN <= PIX_4_7(7 downto 4) & "00";
                    BLU <= PIX_4_7(3 downto 0) & '0';
                end if;

                -- Row 5
                if ((vc > (0 + (60 * 5))) and (vc < (60 + (60 * 5))) and (hc > (80 + (60 * 0))) and (hc < (140 + (60 * 0)))) then
                    RED <= PIX_5_0(11 downto 8) & '0';
                    GRN <= PIX_5_0(7 downto 4) & "00";
                    BLU <= PIX_5_0(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 5))) and (vc < (60 + (60 * 5))) and (hc > (80 + (60 * 1))) and (hc < (140 + (60 * 1)))) then
                    RED <= PIX_5_1(11 downto 8) & '0';
                    GRN <= PIX_5_1(7 downto 4) & "00";
                    BLU <= PIX_5_1(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 5))) and (vc < (60 + (60 * 5))) and (hc > (80 + (60 * 2))) and (hc < (140 + (60 * 2)))) then
                    RED <= PIX_5_2(11 downto 8) & '0';
                    GRN <= PIX_5_2(7 downto 4) & "00";
                    BLU <= PIX_5_2(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 5))) and (vc < (60 + (60 * 5))) and (hc > (80 + (60 * 3))) and (hc < (140 + (60 * 3)))) then
                    RED <= PIX_5_3(11 downto 8) & '0';
                    GRN <= PIX_5_3(7 downto 4) & "00";
                    BLU <= PIX_5_3(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 5))) and (vc < (60 + (60 * 5))) and (hc > (80 + (60 * 4))) and (hc < (140 + (60 * 4)))) then
                    RED <= PIX_5_4(11 downto 8) & '0';
                    GRN <= PIX_5_4(7 downto 4) & "00";
                    BLU <= PIX_5_4(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 5))) and (vc < (60 + (60 * 5))) and (hc > (80 + (60 * 5))) and (hc < (140 + (60 * 5)))) then
                    RED <= PIX_5_5(11 downto 8) & '0';
                    GRN <= PIX_5_5(7 downto 4) & "00";
                    BLU <= PIX_5_5(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 5))) and (vc < (60 + (60 * 5))) and (hc > (80 + (60 * 6))) and (hc < (140 + (60 * 6)))) then
                    RED <= PIX_5_6(11 downto 8) & '0';
                    GRN <= PIX_5_6(7 downto 4) & "00";
                    BLU <= PIX_5_6(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 5))) and (vc < (60 + (60 * 5))) and (hc > (80 + (60 * 7))) and (hc < (140 + (60 * 7)))) then
                    RED <= PIX_5_7(11 downto 8) & '0';
                    GRN <= PIX_5_7(7 downto 4) & "00";
                    BLU <= PIX_5_7(3 downto 0) & '0';
                end if;

                -- Row 6
                if ((vc > (0 + (60 * 6))) and (vc < (60 + (60 * 6))) and (hc > (80 + (60 * 0))) and (hc < (140 + (60 * 0)))) then
                    RED <= PIX_6_0(11 downto 8) & '0';
                    GRN <= PIX_6_0(7 downto 4) & "00";
                    BLU <= PIX_6_0(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 6))) and (vc < (60 + (60 * 6))) and (hc > (80 + (60 * 1))) and (hc < (140 + (60 * 1)))) then
                    RED <= PIX_6_1(11 downto 8) & '0';
                    GRN <= PIX_6_1(7 downto 4) & "00";
                    BLU <= PIX_6_1(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 6))) and (vc < (60 + (60 * 6))) and (hc > (80 + (60 * 2))) and (hc < (140 + (60 * 2)))) then
                    RED <= PIX_6_2(11 downto 8) & '0';
                    GRN <= PIX_6_2(7 downto 4) & "00";
                    BLU <= PIX_6_2(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 6))) and (vc < (60 + (60 * 6))) and (hc > (80 + (60 * 3))) and (hc < (140 + (60 * 3)))) then
                    RED <= PIX_6_3(11 downto 8) & '0';
                    GRN <= PIX_6_3(7 downto 4) & "00";
                    BLU <= PIX_6_3(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 6))) and (vc < (60 + (60 * 6))) and (hc > (80 + (60 * 4))) and (hc < (140 + (60 * 4)))) then
                    RED <= PIX_6_4(11 downto 8) & '0';
                    GRN <= PIX_6_4(7 downto 4) & "00";
                    BLU <= PIX_6_4(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 6))) and (vc < (60 + (60 * 6))) and (hc > (80 + (60 * 5))) and (hc < (140 + (60 * 5)))) then
                    RED <= PIX_6_5(11 downto 8) & '0';
                    GRN <= PIX_6_5(7 downto 4) & "00";
                    BLU <= PIX_6_5(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 6))) and (vc < (60 + (60 * 6))) and (hc > (80 + (60 * 6))) and (hc < (140 + (60 * 6)))) then
                    RED <= PIX_6_6(11 downto 8) & '0';
                    GRN <= PIX_6_6(7 downto 4) & "00";
                    BLU <= PIX_6_6(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 6))) and (vc < (60 + (60 * 6))) and (hc > (80 + (60 * 7))) and (hc < (140 + (60 * 7)))) then
                    RED <= PIX_6_7(11 downto 8) & '0';
                    GRN <= PIX_6_7(7 downto 4) & "00";
                    BLU <= PIX_6_7(3 downto 0) & '0';
                end if;

                -- Row 7
                if ((vc > (0 + (60 * 7))) and (vc < (60 + (60 * 7))) and (hc > (80 + (60 * 0))) and (hc < (140 + (60 * 0)))) then
                    RED <= PIX_7_0(11 downto 8) & '0';
                    GRN <= PIX_7_0(7 downto 4) & "00";
                    BLU <= PIX_7_0(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 7))) and (vc < (60 + (60 * 7))) and (hc > (80 + (60 * 1))) and (hc < (140 + (60 * 1)))) then
                    RED <= PIX_7_1(11 downto 8) & '0';
                    GRN <= PIX_7_1(7 downto 4) & "00";
                    BLU <= PIX_7_1(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 7))) and (vc < (60 + (60 * 7))) and (hc > (80 + (60 * 2))) and (hc < (140 + (60 * 2)))) then
                    RED <= PIX_7_2(11 downto 8) & '0';
                    GRN <= PIX_7_2(7 downto 4) & "00";
                    BLU <= PIX_7_2(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 7))) and (vc < (60 + (60 * 7))) and (hc > (80 + (60 * 3))) and (hc < (140 + (60 * 3)))) then
                    RED <= PIX_7_3(11 downto 8) & '0';
                    GRN <= PIX_7_3(7 downto 4) & "00";
                    BLU <= PIX_7_3(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 7))) and (vc < (60 + (60 * 7))) and (hc > (80 + (60 * 4))) and (hc < (140 + (60 * 4)))) then
                    RED <= PIX_7_4(11 downto 8) & '0';
                    GRN <= PIX_7_4(7 downto 4) & "00";
                    BLU <= PIX_7_4(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 7))) and (vc < (60 + (60 * 7))) and (hc > (80 + (60 * 5))) and (hc < (140 + (60 * 5)))) then
                    RED <= PIX_7_5(11 downto 8) & '0';
                    GRN <= PIX_7_5(7 downto 4) & "00";
                    BLU <= PIX_7_5(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 7))) and (vc < (60 + (60 * 7))) and (hc > (80 + (60 * 6))) and (hc < (140 + (60 * 6)))) then
                    RED <= PIX_7_6(11 downto 8) & '0';
                    GRN <= PIX_7_6(7 downto 4) & "00";
                    BLU <= PIX_7_6(3 downto 0) & '0';
                end if;

                if ((vc > (0 + (60 * 7))) and (vc < (60 + (60 * 7))) and (hc > (80 + (60 * 7))) and (hc < (140 + (60 * 7)))) then
                    RED <= PIX_7_7(11 downto 8) & '0';
                    GRN <= PIX_7_7(7 downto 4) & "00";
                    BLU <= PIX_7_7(3 downto 0) & '0';
                end if;

            else
                RED <= "00000";
                GRN <= "000000";
                BLU <= "00000";
            end if;
        end if;

    end process;

end Behavioral;