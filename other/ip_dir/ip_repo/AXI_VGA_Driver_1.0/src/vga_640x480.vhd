library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity vga_640x480 is	
	port 
	( 
		clk   : in std_logic;

		hsync : out std_logic;
		vsync : out std_logic;		
		hc    : out std_logic_vector(9 downto 0);
		vc    : out std_logic_vector(9 downto 0);
		vidon : out std_logic	
	);
end vga_640x480;

architecture Behavioral of vga_640x480 is
	-- Number of pixels in a horizontal line = 800
	constant hpixels : std_logic_vector(9 downto 0) := "1100100000";
	-- Number of horizontal lines in the display = 525
	constant vlines  : std_logic_vector(9 downto 0) := "1000001101";
	-- Horizontal back porch = 144 (128 + 16)
	constant hbp     : std_logic_vector(9 downto 0) := "0010010000";
	-- Horizontal front porch = 752 (128 + 16 + 640)
	constant hfp     : std_logic_vector(9 downto 0) := "1011110000";
	-- Vertical back porch = 31 (2 + 33)
	constant vbp     : std_logic_vector(9 downto 0) := "0000100011";
	-- Vertical font porch = 492 (2 + 10 + 480)
	constant vfp     : std_logic_vector(9 downto 0) := "0111101100";
	
	-- Horizontal counter
	signal hcs : std_logic_vector(9 downto 0);
	-- Vertical counter
	signal vcs : std_logic_vector(9 downto 0);
	
	-- Enable for the vertical counter
	signal vsenable : std_logic;

begin
	-- Horizontal counter process
	h_counter : process(clk)
	begin
		if (rising_edge(clk)) then
			-- The counter has reached the end of the pixel count
			if (hcs = (hpixels - 1)) then 
				-- Reset the counter
				hcs <= (others => '0');
				-- Enable the vertical counter
				vsenable  <= '1';
			else
				-- Increment the horizontal counter
				hcs <= hcs + 1;
				-- Leave the vsenable off
				vsenable <= '0';
			end if;
		end if;
	end process;
	
	-- Horizontal sync pulse is low when hc is 0 -> 127
	hsync <= '0' when hcs < 128 else '1';
	
	-- Vertical counter process
	v_counter : process(clk)
	begin
		-- Increment the counter when enabled
		if (rising_edge(clk) AND vsenable ='1') then
			-- Reset when the number of lines is reached
			if (vcs = (vlines - 1)) then
				vcs <= (others => '0');
			else
				-- Increment the vertical counter
				vcs <= vcs + 1;
			end if;
		end if;
	end process;

	-- Vertical sync pulse is low when vc is 0 or 1
	vsync <= '0' when vcs < 2 else '1';
	
	-- Enable video out when within the porches
	vidon <= '1' when (((hcs < hfp) AND (hcs >= hbp))
				AND ((vcs < vfp) AND (vcs >= vbp))) else '0';
	
	-- Output horizontal and vertical counters
	hc <= hcs;
	vc <= vcs;
	
end Behavioral;
