----------------------------------------------------------------------------------
-- Company:     Hogeschool PXL
-- Engineer:    Vincent claes
-- 
-- Create Date: 27.11.2017 10:20:30
-- Design Name: 
-- Module Name: WS2812 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- by Vincent Claes
-- vincent.claes[at]pxl.be
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity WS2812x is
    generic
    (
        clock_frequency : integer := 50_000_000 -- Hertz
    );
    port
    (
        clk       : in std_logic;
        runstates : in std_logic_vector(3 downto 0);

        d_out     : out std_logic;
        feedback  : out std_logic_vector(31 downto 0);

        -- AXI registers
        PIX_0_0   : in std_logic_vector(23 downto 0);
        PIX_0_1   : in std_logic_vector(23 downto 0);
        PIX_0_2   : in std_logic_vector(23 downto 0);
        PIX_0_3   : in std_logic_vector(23 downto 0);
        PIX_0_4   : in std_logic_vector(23 downto 0);
        PIX_0_5   : in std_logic_vector(23 downto 0);
        PIX_0_6   : in std_logic_vector(23 downto 0);
        PIX_0_7   : in std_logic_vector(23 downto 0);
        PIX_1_0   : in std_logic_vector(23 downto 0);
        PIX_1_1   : in std_logic_vector(23 downto 0);
        PIX_1_2   : in std_logic_vector(23 downto 0);
        PIX_1_3   : in std_logic_vector(23 downto 0);
        PIX_1_4   : in std_logic_vector(23 downto 0);
        PIX_1_5   : in std_logic_vector(23 downto 0);
        PIX_1_6   : in std_logic_vector(23 downto 0);
        PIX_1_7   : in std_logic_vector(23 downto 0);
        PIX_2_0   : in std_logic_vector(23 downto 0);
        PIX_2_1   : in std_logic_vector(23 downto 0);
        PIX_2_2   : in std_logic_vector(23 downto 0);
        PIX_2_3   : in std_logic_vector(23 downto 0);
        PIX_2_4   : in std_logic_vector(23 downto 0);
        PIX_2_5   : in std_logic_vector(23 downto 0);
        PIX_2_6   : in std_logic_vector(23 downto 0);
        PIX_2_7   : in std_logic_vector(23 downto 0);
        PIX_3_0   : in std_logic_vector(23 downto 0);
        PIX_3_1   : in std_logic_vector(23 downto 0);
        PIX_3_2   : in std_logic_vector(23 downto 0);
        PIX_3_3   : in std_logic_vector(23 downto 0);
        PIX_3_4   : in std_logic_vector(23 downto 0);
        PIX_3_5   : in std_logic_vector(23 downto 0);
        PIX_3_6   : in std_logic_vector(23 downto 0);
        PIX_3_7   : in std_logic_vector(23 downto 0);
        PIX_4_0   : in std_logic_vector(23 downto 0);
        PIX_4_1   : in std_logic_vector(23 downto 0);
        PIX_4_2   : in std_logic_vector(23 downto 0);
        PIX_4_3   : in std_logic_vector(23 downto 0);
        PIX_4_4   : in std_logic_vector(23 downto 0);
        PIX_4_5   : in std_logic_vector(23 downto 0);
        PIX_4_6   : in std_logic_vector(23 downto 0);
        PIX_4_7   : in std_logic_vector(23 downto 0);
        PIX_5_0   : in std_logic_vector(23 downto 0);
        PIX_5_1   : in std_logic_vector(23 downto 0);
        PIX_5_2   : in std_logic_vector(23 downto 0);
        PIX_5_3   : in std_logic_vector(23 downto 0);
        PIX_5_4   : in std_logic_vector(23 downto 0);
        PIX_5_5   : in std_logic_vector(23 downto 0);
        PIX_5_6   : in std_logic_vector(23 downto 0);
        PIX_5_7   : in std_logic_vector(23 downto 0);
        PIX_6_0   : in std_logic_vector(23 downto 0);
        PIX_6_1   : in std_logic_vector(23 downto 0);
        PIX_6_2   : in std_logic_vector(23 downto 0);
        PIX_6_3   : in std_logic_vector(23 downto 0);
        PIX_6_4   : in std_logic_vector(23 downto 0);
        PIX_6_5   : in std_logic_vector(23 downto 0);
        PIX_6_6   : in std_logic_vector(23 downto 0);
        PIX_6_7   : in std_logic_vector(23 downto 0);
        PIX_7_0   : in std_logic_vector(23 downto 0);
        PIX_7_1   : in std_logic_vector(23 downto 0);
        PIX_7_2   : in std_logic_vector(23 downto 0);
        PIX_7_3   : in std_logic_vector(23 downto 0);
        PIX_7_4   : in std_logic_vector(23 downto 0);
        PIX_7_5   : in std_logic_vector(23 downto 0);
        PIX_7_6   : in std_logic_vector(23 downto 0);
        PIX_7_7   : in std_logic_vector(23 downto 0)
    );
end WS2812x;

architecture Behavioral of WS2812x is

    constant T0_h  : integer := 17;
    constant T0_l  : integer := 38;
    constant T1_h  : integer := 35;
    constant T1_l  : integer := 28;
    constant Reset : integer := 2500;

    type LED_matrix is array(0 to 63) of std_logic_vector(23 downto 0);
    type st_machine is (loading_state, sending_state, send_bit_state, reset_state);

begin
    process
        variable state           : st_machine                    := loading_state;
        variable GRB             : std_logic_vector(23 downto 0) := x"000000";
        variable delay_high_cntr : integer                       := 0;
        variable delay_low_cntr  : integer                       := 0;
        variable index           : integer                       := 0;
        variable bit_cntr        : integer                       := 0;
        variable LEDS            : LED_matrix;

    begin
            LEDS(0)  := PIX_0_0;
            LEDS(1)  := PIX_0_1;
            LEDS(2)  := PIX_0_2;
            LEDS(3)  := PIX_0_3;
            LEDS(4)  := PIX_0_4;
            LEDS(5)  := PIX_0_5;
            LEDS(6)  := PIX_0_6;
            LEDS(7)  := PIX_0_7;
            LEDS(8)  := PIX_1_0;
            LEDS(9)  := PIX_1_1;
            LEDS(10) := PIX_1_2;
            LEDS(11) := PIX_1_3;
            LEDS(12) := PIX_1_4;
            LEDS(13) := PIX_1_5;
            LEDS(14) := PIX_1_6;
            LEDS(15) := PIX_1_7;
            LEDS(16) := PIX_2_0;
            LEDS(17) := PIX_2_1;
            LEDS(18) := PIX_2_2;
            LEDS(19) := PIX_2_3;
            LEDS(20) := PIX_2_4;
            LEDS(21) := PIX_2_5;
            LEDS(22) := PIX_2_6;
            LEDS(23) := PIX_2_7;
            LEDS(24) := PIX_3_0;
            LEDS(25) := PIX_3_1;
            LEDS(26) := PIX_3_2;
            LEDS(27) := PIX_3_3;
            LEDS(28) := PIX_3_4;
            LEDS(29) := PIX_3_5;
            LEDS(30) := PIX_3_6;
            LEDS(31) := PIX_3_7;
            LEDS(32) := PIX_4_0;
            LEDS(33) := PIX_4_1;
            LEDS(34) := PIX_4_2;
            LEDS(35) := PIX_4_3;
            LEDS(36) := PIX_4_4;
            LEDS(37) := PIX_4_5;
            LEDS(38) := PIX_4_6;
            LEDS(39) := PIX_4_7;
            LEDS(40) := PIX_5_0;
            LEDS(41) := PIX_5_1;
            LEDS(42) := PIX_5_2;
            LEDS(43) := PIX_5_3;
            LEDS(44) := PIX_5_4;
            LEDS(45) := PIX_5_5;
            LEDS(46) := PIX_5_6;
            LEDS(47) := PIX_5_7;
            LEDS(48) := PIX_6_0;
            LEDS(49) := PIX_6_1;
            LEDS(50) := PIX_6_2;
            LEDS(51) := PIX_6_3;
            LEDS(52) := PIX_6_4;
            LEDS(53) := PIX_6_5;
            LEDS(54) := PIX_6_6;
            LEDS(55) := PIX_6_7;
            LEDS(56) := PIX_7_0;
            LEDS(57) := PIX_7_1;
            LEDS(58) := PIX_7_2;
            LEDS(59) := PIX_7_3;
            LEDS(60) := PIX_7_4;
            LEDS(61) := PIX_7_5;
            LEDS(62) := PIX_7_6;
            LEDS(63) := PIX_7_7;
            
            wait until rising_edge(clk);
            if (runstates = "0001") then

            case state is
                when loading_state =>
                    GRB      := LEDS(index);
                    bit_cntr := 24;
                    state    := sending_state;
                    feedback(0) <= '0';
                when sending_state =>
                    if (bit_cntr > 0) then
                        bit_cntr := bit_cntr - 1;
                        if GRB(bit_cntr) = '1' then
                            delay_high_cntr := T1_h;
                            delay_low_cntr  := T1_l;
                        else
                            delay_high_cntr := T0_h;
                            delay_low_cntr  := T0_l;
                        end if;
                        state := send_bit_state;
                    else
                        if (index < 63) then
                            index := index + 1;
                            state := loading_state;
                        else
                            delay_low_cntr := Reset;
                            state          := reset_state;
                        end if;
                    end if;
                when send_bit_state =>
                    if (delay_high_cntr > 0) then
                        d_out <= '1';
                        delay_high_cntr := delay_high_cntr - 1;
                    elsif (delay_low_cntr > 0) then
                        d_out <= '0';
                        delay_low_cntr := delay_low_cntr - 1;
                    else
                        state := sending_state;
                    end if;
                when reset_state =>
                    if (delay_low_cntr > 0) then
                        d_out <= '0';
                        delay_low_cntr := delay_low_cntr - 1;
                    else
                        index := 0;
                        feedback(0) <= '1';
                    end if;
                when others => NULL;
            end case;
        else
            feedback(0) <= '0';
            state := loading_state;
        end if;
    end process;
end Behavioral;