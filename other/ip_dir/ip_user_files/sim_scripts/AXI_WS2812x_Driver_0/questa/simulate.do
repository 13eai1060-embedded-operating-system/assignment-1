onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib AXI_WS2812x_Driver_0_opt

do {wave.do}

view wave
view structure
view signals

do {AXI_WS2812x_Driver_0.udo}

run -all

quit -force
