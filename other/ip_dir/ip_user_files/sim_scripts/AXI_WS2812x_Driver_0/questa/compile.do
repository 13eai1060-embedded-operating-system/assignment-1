vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xil_defaultlib

vmap xil_defaultlib questa_lib/msim/xil_defaultlib

vcom -work xil_defaultlib -64 -93 \
"../../../../AXI_WS2812x_Driver_0/hdl/AXI_WS2812x_Driver_v1_0_S00_AXI.vhd" \
"../../../../AXI_WS2812x_Driver_0/hdl/WS2812x.vhd" \
"../../../../AXI_WS2812x_Driver_0/hdl/AXI_WS2812x_Driver_v1_0.vhd" \
"../../../../AXI_WS2812x_Driver_0/sim/AXI_WS2812x_Driver_0.vhd" \


