/*
 * includes.c
 *
 *  Created on: Jan 6, 2019
 *      Author: bryan
 */

#include "includes.h"

/*
 * Initialize the frame buffer array to all 0 to avoid undefined behavior
 */
void
init_array(uint32_t pixel_array[SCREEN_HEIGHT][SCREEN_WIDTH], uint32_t color)
{
	uint32_t i = 0;
	uint32_t j = 0;

	for (i = 0; i < SCREEN_HEIGHT; i++)
	{
		for (j = 0; j < SCREEN_WIDTH; j++)
		{
			pixel_array[i][j] = color;
		}
	}

	return;
}

void
update_screens(uint32_t pixel_array[SCREEN_HEIGHT][SCREEN_WIDTH])
{
	uint32_t i = 0u;
	uint32_t j = 0u;

	uint32_t addr = 0u;

	Xil_Out32((NEOPIXEL_BASE_ADDR + AXI_WS2812X_DRIVER_S00_AXI_SLV_REG64_OFFSET), 0b0000);

	for (i = 0u; i < SCREEN_HEIGHT; i++)
	{
		for (j = 0u; j < SCREEN_WIDTH; j++)
		{
			/* Offset address calculation */
			addr = ((4u * i) + (32u * j));

			/* NeoPixel part */
			Xil_Out32((NEOPIXEL_BASE_ADDR + addr), RGBtoGRB(pixel_array[i][j]));

			/* VGA part */
			/* Xil_Out32((VGA_BASE_ADDR + addr), pixel_array[i][j]); */
		}
	}
	Xil_Out32((NEOPIXEL_BASE_ADDR + AXI_WS2812X_DRIVER_S00_AXI_SLV_REG64_OFFSET), 0b0001);

	return;
}

/*
 * VGA bit depth = 12
 * NeoPixel bit depth = 24
 * That is the reason there is a cluster fuck of bit shifts here
 * To get 12 bits into 24
 * Bits have been chosen in such a way maximum brightness is always achieved.
 */
uint32_t
RGBtoGRB(uint32_t RGB)
{
	uint32_t GRB = 0u;
	uint32_t R = 0u;
	uint32_t G = 0u;
	uint32_t B = 0u;

	uint32_t R_mask = 0xF00u;
	uint32_t G_mask = 0x0F0u;
	uint32_t B_mask = 0x00Fu;

	R = (RGB & R_mask) >> 8u;
	G = (RGB & G_mask) >> 4u;
	B = (RGB & B_mask);

#ifdef _DEBUG
	xil_printf("0x%X | 0x%X | 0x%X\r\n", R, G, B);
#endif

	GRB = (G << 20u) | (R << 12u) | (B << 4u);

#ifdef _DEBUG
	xil_printf("0x%X | 0x%X | 0x%X = 0x%X\r\n", G << 20u, R << 12u, B << 4u, GRB);
#endif
	return GRB;
}
